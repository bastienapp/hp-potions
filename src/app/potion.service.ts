import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PotionService {

  constructor() { }

  async getAllPotions() {
    const result = await fetch('https://api.potterdb.com/v1/potions')
      .then((response) => response.json())
      .then((apiResponse: ApiResponse) => {
        // si vous voulez transformer des données d'une API, faites un .map !
        return apiResponse.data.map((element: ApiPotion): Potion => {
          return {
            id: element.id,
            ...element.attributes
          }
        });
      });

    return result;
  }
}

export type Potion = {
  id: string,
  name: string,
  ingredients: string
}

type ApiPotion = {
  id: string,
  attributes: {
    name: string,
    ingredients: string
  }
}

type ApiResponse = {
  data: ApiPotion[]
}
