import { Component } from '@angular/core';
import { Potion, PotionService } from '../potion.service';



@Component({
  selector: 'app-potion-list',
  standalone: true,
  imports: [],
  templateUrl: './potion-list.component.html',
  styleUrl: './potion-list.component.css'
})
export class PotionListComponent {

  potionList: Potion[] = [];
  potionService: PotionService;

  constructor(potionServiceInjected: PotionService) {
    // ici c'est que pour l'injection de dépendances
    this.potionService = potionServiceInjected;
  }

  ngOnInit(): void {
    // montage du composant
    this.potionService.getAllPotions()
      .then((response) => {
        this.potionList = response;
      })
  }
}
