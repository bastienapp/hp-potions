import { Routes } from '@angular/router';
import { PotionListComponent } from './potion-list/potion-list.component';

export const routes: Routes = [
  { path: '', component: PotionListComponent }
];
